﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Assessment1_10004251
{
    public class class1
    {
        public static int Result(string number)

        {
            int i,
            a, //multiples of 3
            b, //multiples of 5
            c,
            Sum = 0;

            i = int.Parse(number);

            for (c = 1; c < i; c++)
            {
                a = c % 3;
                b = c % 5;
                if (a == 0 || b == 0)
                {

                    Sum = Sum + c;
                }
            }
            return Sum;
        }
    }
}